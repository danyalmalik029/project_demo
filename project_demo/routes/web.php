<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $reponse=file_get_contents('data.json');
    $temp=json_decode($reponse);
//    dd($temp);
//    if(isset($temp)) {
//        foreach ($temp as $t) {
//            dd($t->product);
//        }
//    }
    return view('product')->with('data',$temp);
});

Route::get('/ajax_add_product', 'HomeController@ajax_add_product')->name('ajax_add_product');
Route::get('/ajax_edit_product', 'HomeController@ajax_edit_product')->name('ajax_edit_product');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
