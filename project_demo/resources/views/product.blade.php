@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class=" col-md-3 form-group">
                                <label>Product name
                                    <input type="text" class="form-control" id="product" name="product">
                                </label>
                            </div>
                            <div class=" col-md-3 form-group">
                                <label>Quantity in stock
                                    <input type="number" class="form-control" id="quantity_stock" name="quantity_stock">
                                </label>
                            </div>
                            <div class=" col-md-3 form-group">
                                <label>Price per item
                                    <input type="number" class="form-control" id="price_per_item" name="price_per_item">
                                </label>
                            </div>
                            <div class=" col-md-3 ">
                                <button type="button" onclick="submit()" class="btn btn-primary" style="margin-top: 20px">Submit</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Quantity in stock</th>
                                        <th>Price Per Item</th>
                                        <th>DateTime</th>
                                        <th>Total Value No</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="add_table">
                                    @if(isset($data))
                                        @foreach($data as $temp)
                                            <tr>
                                                <td>{{$temp->product}}</td>
                                                <td>{{$temp->quantity_stock}}</td>
                                                <td>{{$temp->price_per_item}}</td>
                                                <td>{{$temp->date}}</td>
                                                <td>{{$temp->quantity_stock*$temp->price_per_item}}</td>
                                                <td>
                                                {{--<button type="button" class="btn btn-success" >Edit</button>--}}
                                                    <button type="button" class="btn btn-success" onclick="edit('{{$temp->product}}','{{$temp->quantity_stock}}','{{$temp->price_per_item}}')" data-toggle="modal" data-target="#myModal">Edit</button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Products</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class=" col-md-3 form-group">
                            <label>Product name
                                <input type="text" class="form-control" id="product_edit" name="product">
                            </label>
                        </div>
                        <div class=" col-md-3 form-group">
                            <label>Quantity in stock
                                <input type="number" class="form-control" id="quantity_stock_edit" name="quantity_stock">
                            </label>
                        </div>
                        <div class=" col-md-3 form-group">
                            <label>Price per item
                                <input type="number" class="form-control" id="price_per_item_edit" name="price_per_item">
                            </label>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="edit_submit()" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@endsection
<script>
    var product_edit="";
    var quantity_stock_edit="";
    var price_per_item_edit="";
    function submit() {
        var product=$('#product').val();
        var quantity_stock=$('#quantity_stock').val();
        var price_per_item=$('#price_per_item').val();
        var token = '{{ csrf_token() }}';
        $('#product').val("");
        $('#quantity_stock').val("");
        $('#price_per_item').val("");
        {{--var _token={{ csrf_field() }};--}}
        alert(product+' '+quantity_stock+" "+price_per_item);
        $.ajax({

            url:"{{route('ajax_add_product')}}",
            method: "get",
//            data:{_token:token,id:1},
            data: {_token:token,product: product,quantity_stock:quantity_stock,price_per_item:price_per_item},

            success: function (data) {
                alert(data.length);
                $('#add_table').empty();
                for(var x=0;x<data.length;x++)
                {
                                    $('#add_table').append(' <tr>\n' +
                    '                                    <td>'+data[x]["product"]+'</td>\n' +
                    '                                    <td>'+data[x]["quantity_stock"]+'</td>\n' +
                    '                                    <td>'+data[x]["price_per_item"]+'</td>\n' +
                    '                                    <td>'+data[x]["date"]+'</td>\n' +
                    '                                    <td>'+data[x]["quantity_stock"]*data[x]["price_per_item"]+'</td>\n' +
                                        '                                    <td><button type="button" class="btn btn-success" onclick="edit('+data[x]["product"]+','+data[x]["quantity_stock"]+','+data[x]["price_per_item"]+')" data-toggle="modal" data-target="#myModal">Edit</button></td>\n' +
                    //                     '                                    <td>'+data["sdfsd"]+'</td>\n' +
                    '                                </tr>');
                }

            }
        });
    }

    function  edit(product,quantity_stock,price_per_item) {
//        alert(product+' '+quantity_stock+" "+price_per_item);
        product_edit=product;
            $('#product_edit').val(product);
        quantity_stock_edit=quantity_stock;
            $('#quantity_stock_edit').val(quantity_stock);
        price_per_item_edit=price_per_item;
            $('#price_per_item_edit').val(price_per_item);
//        alert(product_edit+' '+quantity_stock_edit+" "+price_per_item_edit);
    }


    function edit_submit() {
//        $('#myModal').modal('close');
        var product=$('#product_edit').val();
        var quantity_stock=$('#quantity_stock_edit').val();
        var price_per_item=$('#price_per_item_edit').val();
        var token = '{{ csrf_token() }}';
//        alert(product_edit+' '+quantity_stock_edit+" "+price_per_item_edit);
        {{--var _token={{ csrf_field() }};--}}
        $.ajax({

            url:"{{route('ajax_edit_product')}}",
            method: "get",
//            data:{_token:token,id:1},
            data: {_token:token,product: product,quantity_stock:quantity_stock,price_per_item:price_per_item,
                product_edit:product_edit,quantity_stock_edit:quantity_stock_edit,price_per_item_edit:price_per_item_edit},

            success: function (data) {
//                alert(data.length);
                $('#add_table').empty();
                for(var x=0;x<data.length;x++)
                {
                    $('#add_table').append(' <tr>\n' +
                        '                                    <td>'+data[x]["product"]+'</td>\n' +
                        '                                    <td>'+data[x]["quantity_stock"]+'</td>\n' +
                        '                                    <td>'+data[x]["price_per_item"]+'</td>\n' +
                        '                                    <td>'+data[x]["date"]+'</td>\n' +
                        '                                    <td>'+data[x]["quantity_stock"]*data[x]["price_per_item"]+'</td>\n' +
                        '                                    <td><button type="button" class="btn btn-success" onclick="edit('+data[x]["product"]+','+data[x]["quantity_stock"]+','+data[x]["price_per_item"]+')" data-toggle="modal" data-target="#myModal">Edit</button></td>\n' +
                        //                     '                                    <td>'+data["sdfsd"]+'</td>\n' +
                        '                                </tr>');
                }

            }
        });
    }
    {{--{{route('ajax_add_product')}}--}}
</script>;