<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function ajax_add_product(Request $request)
    {
        dd($request->product);
        $temp=array();
        $reponse=file_get_contents('data.json');
        $temp=json_decode($reponse);
//        foreach ($temp as $key=>$value)
//        {
//            if($value->product== $request->product && $value->quantity_stock== $request->quantity_stock && $value->product== $request->product)
//        }
        $data=$request->all();
        $data["date"]=date("Y-m-d H:m:i");
        $temp[]=$data;


        file_put_contents('data.json',json_encode($temp));
//        $temp=json_decode($temp,true);
        $reponse=file_get_contents('data.json');
        $all_data=json_decode($reponse,true);
        return $all_data;
    }


    public function ajax_edit_product(Request $request)
    {
//        dd($request->all());
        $temp=array();
        $reponse=file_get_contents('data.json');
        $temp=json_decode($reponse);
        foreach ($temp as $key=>$value)
        {
            if($value->product== $request->product_edit && $value->quantity_stock== $request->quantity_stock_edit && $value->price_per_item== $request->price_per_item_edit)
            {
//                dd(1);
                $temp[$key]->product=$request->product;
                $temp[$key]->quantity_stock=$request->quantity_stock;
                $temp[$key]->price_per_item=$request->price_per_item_edit;
                $temp[$key]->date=date("Y-m-d H:m:i");
            }
        }


        file_put_contents('data.json',json_encode($temp));
//        $temp=json_decode($temp,true);
        $reponse=file_get_contents('data.json');
        $all_data=json_decode($reponse,true);
        return $all_data;
    }
}
